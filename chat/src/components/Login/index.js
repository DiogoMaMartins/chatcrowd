import React from 'react';
import useStyles from './styles.js';
import { Paper,Grid,Typography,TextField,Button,Link } from '@material-ui/core';
function Login(){
		const classes = useStyles();


	return(
			<Paper className={classes.paper}>
				<Grid container className={classes.root} direction="row" justify="center" alignItems="center" spacing={2}>
					<Grid item xs={12} md={6} lg={6} className={classes.imageSide}>
						<img  alt="Logo" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9d3UH9l22xObgjlmFPclz_zTIda13eNyzag2TopvTdj0eNcKS1g" />
					</Grid>
					<Grid item xs={12} md={6} lg={6} className={classes.loginSide}>
									<Grid container className={classes.root} direction="column" justify="center" alignItems="center" spacing={2}>

										<Grid item xs={12} md={12} lg={12} className={classes.loginSide}>
											<Typography variant="h4" gutterBottom>
									           Member Login
									        </Typography>
										</Grid>

										<Grid item xs={12} md={12} lg={12} className={classes.loginSide}>
											<form className={classes.form} noValidate>
									        <TextField
										        id="filled-email-input"
										        label="Email"
										        className={classes.textField}
										        type="email"
										        name="email"
										        autoComplete="email"
										        margin="normal"
										        fullWidth
										        variant="filled"
										      />

									        <TextField
										        id="filled-password-input"
										        label="Password"
										        className={classes.textField}
										        type="password"
										        autoComplete="current-password"
										        margin="normal"
										        fullWidth
										        variant="filled"
										      />


									          <Button
									            type="submit"
									            fullWidth
									            variant="contained"
									            color="primary"
									            className={classes.submit}
									          >
									            Sign In
									          </Button>
									          <Grid container>
									            <Grid item xs={12}>
									              <Link href="/forgout_password" variant="body2">
									                Forgot password/username?
									              </Link>
									            </Grid>
									            <Grid item xs={12}>
									              <Link href="/signup" variant="body2">
									                {"Don't have an account? Sign Up"}
									              </Link>
									            </Grid>
									          </Grid>
									          </form>


										</Grid>

								     </Grid>



					</Grid>
				</Grid>
			</Paper>
		)
}
export default Login;
