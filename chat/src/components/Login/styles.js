import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
  	top:100,
  	position:'relative',
    minHeight: 440,
  },
  imageSide:{
  	display:'flex',
  	flexDirection:'row',
 	justifyContent:'center',
  	alignItems:'center'
  },
  loginSide:{
  	display:'flex',
  	flexDirection:'column',
 	justifyContent:'space-between',
  	alignItems:'center'
  },
  form:{
  	 width: '90%', 
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));