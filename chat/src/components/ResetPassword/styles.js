import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  root: {
    flexGrow: 1,
    display:'flex',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around',
    minWidth:340
  },
  paper: {
  	top:100,
  	position:'relative',
    minHeight: 440,
    minWidth:340,
    display:'flex',
    alignItems:'center',
    justifyContent:'center'
  },
  title:{
    color:'#001f3f',
    textAlign:'center',
    fontWeight:'bold'
  },
  form:{
      width:'90%',
  	  alignSelf:'center'
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));
