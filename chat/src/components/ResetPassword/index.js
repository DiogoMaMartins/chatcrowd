import React from 'react';
import useStyles from './styles.js';
import { Paper,Grid,Typography,TextField,Button,Link } from '@material-ui/core';



function Login(){
		const classes = useStyles();


	return(
			<Paper className={classes.paper}>
							<form className={classes.form} noValidate>
				<Grid container className={classes.root} direction="row" justify="center" alignItems="center" spacing={2}>

					<Grid item xs={12} md={12} lg={12}>

                      <Typography variant="h4"  gutterBottom className={classes.title}>
                             Forgot Password
                          </Typography>

                          <Typography variant="subtitle1" gutterBottom>
                                 If you've lost password or wish to reset it use this form below to get started.
                              </Typography>



													<TextField
										        id="filled-email-input"
										        label="Your email address"
										        className={classes.textField}
										        type="email"
										        name="email"
										        autoComplete="email"
										        margin="normal"
										        fullWidth
										        variant="filled"
										      />


                          <Button
                						type="submit"
                						fullWidth
                						variant="contained"
                						color="primary"
                						className={classes.submit}
                					>
                					     Reset Password
                					</Button>

                          <Grid container>
                            <Grid item xs={12} md={6} lg={6}>
                              <Link href="/" variant="body2">
                                {"You have an account? Sign In"}
                              </Link>
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                              <Link href="/signup" variant="body2">
                                {"Don't have an account? Sign Up"}
                              </Link>
                            </Grid>
                          </Grid>
					</Grid>


				</Grid>

					  </form>
			</Paper>
		)
}
export default Login;
