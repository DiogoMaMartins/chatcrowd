import React from 'react';
import useStyles from './styles.js';
import { Paper,Grid,TextField,Button,Link } from '@material-ui/core';


const sexGender = [
  {
    value: 'Female',
    label: 'Female',
  },
  {
    value: 'Male',
    label: 'Male',
  },
];


function Login(){
		const classes = useStyles();
		const [values, setValues] = React.useState({
	    showPassword: false,
			gender: 'Male',
	  });

		const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };


	return(
			<Paper className={classes.paper}>
							<form className={classes.form} noValidate>
				<Grid container className={classes.root} direction="row" justify="center" alignItems="center" spacing={2}>

					<Grid item xs={12} md={6} lg={6}>

							<TextField
								id="filled-email-input"
								label="Firstname"
								className={classes.textField}
								type="email"
								name="Firstname"
								autoComplete="firstname"
								margin="normal"
								fullWidth
								variant="filled"
							/>

							<TextField
							fullWidth
        id="filled-select-currency-native"
        select
        label="Gender"
        className={classes.textField}
        value={values.gender}
        onChange={handleChange('gender')}
        SelectProps={{
          native: true,
          MenuProps: {
            className: classes.menu,
          },
        }}
        margin="normal"
        variant="filled"
      >
        {sexGender.map(option => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </TextField>

			<TextField
				id="filled-email-input"
				label="Address"
				className={classes.textField}
				type="email"
				name="Address"
				autoComplete="address"
				margin="normal"
				fullWidth
				variant="filled"
			/>

					</Grid>
					<Grid item xs={12} md={6} lg={6} >
									        <TextField
										        id="filled-email-input"
										        label="Lastname"
										        className={classes.textField}
										        type="email"
										        name="lastname"
										        autoComplete="lastname"
										        margin="normal"
										        fullWidth
										        variant="filled"
										      />
													<TextField
										        id="filled-email-input"
										        label="Email"
										        className={classes.textField}
										        type="email"
										        name="email"
										        autoComplete="email"
										        margin="normal"
										        fullWidth
										        variant="filled"
										      />

									        <TextField
										        id="filled-password-input"
										        label="Password"
										        className={classes.textField}
										        type="password"
										        autoComplete="current-password"
										        margin="normal"
										        fullWidth
										        variant="filled"
										      />

					</Grid>
					<Button
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						className={classes.submit}
					>
						Sign Up
					</Button>
					<Grid container>
						<Grid item xs={12} md={6} lg={6}>
							<Link href="/forgout_password" variant="body2">
								Forgot password/username?
							</Link>
						</Grid>
						<Grid style={{marginBottom:20}} item xs={12} md={6} lg={6}>
							<Link href="/" variant="body2">
								{"You have an account? Sign In"}
							</Link>

						</Grid>
					</Grid>

				</Grid>

					  </form>

			</Paper>
		)
}
export default Login;
