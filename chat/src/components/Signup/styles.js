import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  root: {
    flexGrow: 1,
    display:'flex',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around'
  },
  paper: {
  	top:100,
  	position:'relative',
    minHeight: 460,
    maxWidth:600,
    display:'flex',
    alignItems:'center',
    justifyContent:'center'
  },
  imageSide:{
  	display:'flex',
  	flexDirection:'row',
 	justifyContent:'center',
  	alignItems:'center'
  },
  loginSide:{
  	display:'flex',
  	flexDirection:'column',
 	justifyContent:'space-between',
  	alignItems:'center'
  },
  form:{
      width:'90%',
  	 alignSelf:'center'
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));
