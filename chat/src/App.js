    import React, { Component } from "react";
    import socketIOClient from "socket.io-client";
    import Login from './components/Login';

    class App extends Component {
      constructor() {
        super();
        this.state = {
          response: false,
          endpoint: "http://localhost:4001"
        };
      }
      componentDidMount() {
        const { endpoint } = this.state;
        const socket = socketIOClient(endpoint);
       socket.emit('username', "Diogo");
       socket.on('is_online', (username) => {
                 this.setState({
                   response:username,
                 })
                //$('#messages').append($('<li>').html(username));
            });
	
    
	}
      render() {
        
        return (
          <div style={{ display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
            <Login/>
          </div>
        );
      }
    }
    export default App;
/*
     <div style={{ textAlign: "center" }}>
     const { response } = this.state;
            <Login/>
              {response
                  ? <ul>
                   <li> {response}</li>
                  </ul>
                  : <p>Loading...</p>}
            </div>

    */
