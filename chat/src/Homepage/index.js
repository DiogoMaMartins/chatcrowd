import React, { Component } from "react";
import Login from '../components/Login';

class Homepage extends Component {
  render() {

    return (
      <div style={{ display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
        <Login/>
      </div>
    );
  }
}
export default Homepage;
