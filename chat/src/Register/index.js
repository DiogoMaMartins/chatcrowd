import React, { Component } from "react";
import Signup from '../components/Signup';

class Register extends Component {
  render() {

    return (
      <div style={{ display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
        <h1 style={{color:'white',top:80,position:'relative'}}>Chatcrowd</h1>
        <Signup/>
      </div>
    );
  }
}
export default Register;
