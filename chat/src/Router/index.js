import React, { Component } from 'react';
import { Route,Switch,BrowserRouter as Router } from 'react-router-dom';

import Homepage from '../Homepage';
import Register from '../Register';
import ForgoutPassword from '../ForgoutPassword';


class Routes extends Component{
  render(){
    return(
      <Router>
        <Switch>
          <Route exact path="/" component={Homepage}/>
          <Route exact path="/signup" component={Register}/>
          <Route exact path="/forgout_password" component={ForgoutPassword}/>
        </Switch>
      </Router>
    )
  }
}

export default Routes;
