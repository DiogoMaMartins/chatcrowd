import React, { Component } from "react";
import ResetPassword from '../components/ResetPassword';

class ForgoutPassword extends Component {
  render() {

    return (
      <div style={{ display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
        <h1 style={{color:'white',top:80,position:'relative'}}>Chatcrowd</h1>
        <ResetPassword/>
      </div>
    );
  }
}
export default ForgoutPassword;
