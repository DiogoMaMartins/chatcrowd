'use stict';
const repository = require('../repo/user.js');
const md5 = require('md5');
const authService = require('../middleware/auth');
const toonavatar = require('cartoon-avatar');
const ValidationContract = require('../validators/validator');


exports.get = async(req,res,next) => {
	try{
		let data = await repository.get();
		res.status(200).send(data);
	}catch(e){
		res.status(500).send({
			message:'Error',
			error:e
		})
	}
}

exports.login = async(req,res,next) => {
	try{
		const customer = await repository.login({
			email:req.body.email,
			password:md5(req.body.password + global.SALT_KEY)
		});

		if(!customer){
			res.status(404).send({message:"Email or Password invalid"})
			return;
		}

		const token = await authService.generateToken({
			id:customer._id,
      email:customer.email,
			firstname:customer.firstname,
			lastname:customer.lastname
		})

		res.status(201).send({
			token:token,
			data:{
				email:customer.email,
				firstname:customer.firstname
			}
		});

	}catch(e){
		res.status(500).send({
			message:'error',
			data:e
		})
	}
}

exports.register = async(req,res,next) => {

  let contract = new ValidationContract();
	contract.isEmail(req.body.email,"Please use one valid email");
  contract.hasMinLen(req.body.lastname,3,"Please your lastname need more then 3 caracters");
  contract.hasMinLen(req.body.firstname,3,"Please your firstname need more then 3 caracters");
  contract.hasMinLen(req.body.address,3,"Please your address need more then 3 caracters");
  contract.hasMinLen(req.body.password,3,"Please your password need more then 3 caracters");
  contract.hasMinLen(req.body.gender,3,"Please your gender need more then 3 caracters");

	if (!contract.isValid()){
		res.status(400).send(contract.errors()).end();
		return;
	}

 let url = toonavatar.generate_avatar({"gender":req.body.gender})
	try{
		await repository.register({
			firstname:req.body.firstname,
			lastname:req.body.lastname,
			image:url,
			address:req.body.address,
			email:req.body.email,
			password:md5(req.body.password + global.SALT_KEY),
			gender:req.body.gender
		})

		res.status(201).send({
			message:"user registered successfully"
		})

	}catch(e){
		res.status(500).send({
			message:'Error',
			data:e
		})
	}
}

exports.update = (req,res,next) => {
	repository
		.update(req.body.id,req.body)
		.then(x => {
			res.status(201).send({
				message:'User has been updated successfully'
			});
		})
		.catch(error => {
			res.status(400).send({
				message:'Error',
				data:e
			});
		});
}


exports.delete = (req,res,next) => {
	repository
		.delete(req.params.id)
		.then(x => {
			res.status(200).send({
				message:'User Removed'
			});
		})
		.catch(e => {
			res.status(400).send({
				message:'Error',
				data:e
			});
		});
}
