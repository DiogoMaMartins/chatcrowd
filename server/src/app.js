const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const router = express.Router();
const mongoose = require('mongoose');
const config = require('../bin/config');
const cors = require('cors');
const { version } = require('../package.json');
mongoose.connect(config.database,{
	useNewUrlParser:true,
	useUnifiedTopology: true
})

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
app.set('superNode-auth', config.configName);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended:false
}));

const User = require('./models/user');
const user = require('./routes/user');

app.use('/api/user',user)

app.get('/',(req,res,next) => {
	res.status(200).send({
		title:"Chatcrowd",
		version,
	})
})

module.exports = app;
