const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.get('/', function(req, res) {
  //  res.render('index.ejs');
	res.send("Hello")
});

io.sockets.on('connection', function(socket) {
    socket.on('username', function(username) {
        console.log("username",username)
        socket.username = username;
        io.emit('is_online', '🔵 ' + socket.username + ' join the chat..');
    });

    socket.on('disconnect', function(username) {
        io.emit('is_online', '🔴 <i>' + socket.username + ' left the chat..</i>');
    })

    socket.on('chat_message', function(message) {
        io.emit('chat_message', '<strong>' + socket.username + '</strong>: ' + message);
    });

	 socket.on('subscribeToTimer', (interval) => {
    console.log('client is subscribing to timer with interval ', interval);
    setInterval(() => {
      client.emit('timer', new Date());
    }, interval);
  });
});

const server = http.listen(4001, function() {
    console.log('listening on *:4001');
});
