'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/user');
const authService = require('../middleware/auth');

router.get('/',controller.get);
router.post('/signup',controller.register);
router.post('/signin',controller.login);
router.put('/',controller.update);
router.delete('/delete/:id',controller.delete);

module.exports = router;
