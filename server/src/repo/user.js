const mongoose = require('mongoose');
const User = mongoose.model('User');

exports.get = async(data) => {
	let response = await User.find();
	return response;
}

exports.register = async(data) => {
	let user = new User(data);
	await user.save();
}

exports.login = async(data) => {
	const response = await User.findOne({
		email:data.email,password:data.password
	});
	return response
}

exports.delete = (id) => {
	return User
		.findByIdAndRemove(id)
}

exports.update = (id,data) => {
	console.log('data',data,'id',id)
	return User
		.findByIdAndUpdate(id, {

			$set:{
				firstname:data.firstname,
				lastname:data.lastname,
        gender:data.gender,
				email:data.email,
				address:data.address,
				password:data.password,
			}
		})
}
